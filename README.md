# Dégaine: a simple two-sided timer

Dégaine is a countdown timer. It's made from laser cut wood, an arduino nano (atmega328p), 28 ws2812b leds and a few other components.
The main idea was to build a highly visible timer for when I give conference, facilitate workshop or simply need a timebox.

The countdown is visible from both side of Dégaine, so you and people in front of you can see it.

## Features

You can obviously choose the duration of the timer (in minutes). When running, the timer can be paused and resumed (short press) or cancel (long press).
While running, color will be green at start then change to blue when 50% of the time was spend, yellow at 90% and red when it's done.
Time remaining time is displayed in minutes except for the last 99 seconds where remaining seconds are shown.

When in pause, the display is Orange.

## Plans, code and pcb

### plans

All plans are in svg format and can be found in the [``plans``](plans/) directory.

### code

Code for arduino can be found in the [``arduino``](arduino/) directory.
This project uses [platformIO](https://platformio.org) to manage dependencies and build.

### pcbs:

There are two pcbs: 
* logic_board: it holds the arduino, the rotary encoder and that kind of stuff.
* 7 segments display: all the leds (ws2812) that are use to display stuff.

They are designed with Kicad 5 and can be found in [``kicad``](kicad).

Note: you might need my [kicad library](https://gitlab.com/avernois/kicad-lib).

For both pcb, you'll find a ``plots/gerber.zip`` that contains files required to print those pcb.

#### logic board
* SW2: rotary encoder with click button
* R1, R2, R3: 10kOhm resistors
* P1: 3 pins angled connectors/header (to connect the display board)
* P2: micro usb connector
* U1: arduino nano (with atmega328)

#### 7 segments display
* D1 -> D28: 28 * WS2812b leds
* P1: 3 pins connectors
* P2: is an optional 3 pins connectorss that might be used to chain display. It's currently not need for Dégaine.

## Why "Dégaine"?

Oh, you mean, project names have to mean something?
 
## Licence

The code source is under MIT Licence. Plans and documenation are licenced under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>